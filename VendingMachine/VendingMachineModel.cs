﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VendingMachine
{
    public class VendingMachineModel
    {
        public List<CoinSpecification> AcceptedCoins { get; }

        private List<CoinSpecification> _insertedCoins;

        private decimal _amountSpent;

        public List<Product> AvailableProducts { get; }

        public decimal TotalInsertedAmount => 
            _insertedCoins.Sum(c => c.ValueInDollars) - _amountSpent;


        public VendingMachineModel()
        {
            AcceptedCoins = new List<CoinSpecification>();
            _insertedCoins = new List<CoinSpecification>();

            AvailableProducts = new List<Product>();
        }

        public void AddCoinSpecification(PhysicalCoin coin, decimal value)
        {
            AcceptedCoins.Add(new CoinSpecification
            {
                RadiusInMm = coin.RadiusInMm,
                WeightInGrams = coin.WeightInGrams,
                ValueInDollars = value
            });
        }

        public void AddAvailableProduct(string name, decimal price)
        {
            AvailableProducts.Add(new Product
            {
                Id = AvailableProducts.Count,
                Name = name,
                Price = price
            });
        }

        public bool CoinInserted(PhysicalCoin coin)
        {
            var matchingAcceptedCoins =
                AcceptedCoins.Where(
                    ac => ac.RadiusInMm == coin.RadiusInMm &&
                          ac.WeightInGrams == coin.WeightInGrams).ToList();

            if (!matchingAcceptedCoins.Any())
            {
                return false;
            }

            _insertedCoins.Add(matchingAcceptedCoins.First());

            return true;
        }

        public bool PurchaseProduct(int id)
        {
            var product = AvailableProducts.FirstOrDefault(p => p.Id == id);

            if (product == null)
            {
                throw new Exception("No such product id");
            }

            if (product.Price > TotalInsertedAmount)
            {
                return false;
            }

            _amountSpent += product.Price;

            return true;
        }

        public List<CoinSpecification> ReturnMoney()
        {
            if (_amountSpent == 0)
            {
                var returnCoins = _insertedCoins;

                ResetCoins();

                return returnCoins;
            }

            var change = CalculateChangeCoins();

            ResetCoins();

            return change;
        }

        private List<CoinSpecification> CalculateChangeCoins()
        {
            var coinsDescendingOrderOfValue = AcceptedCoins.OrderByDescending(c => c.ValueInDollars);

            var change = new List<CoinSpecification>();

            foreach (var coin in coinsDescendingOrderOfValue)
            {
                while (change.Sum(c => c.ValueInDollars) + coin.ValueInDollars <= TotalInsertedAmount)
                {
                    change.Add(coin);
                }
            }

            if (change.Sum(c => c.ValueInDollars) != TotalInsertedAmount)
            {
                throw new Exception("Cannot make change from available coins");
            }

            return change;
        }

        private void ResetCoins()
        {
            _insertedCoins = new List<CoinSpecification>();
            _amountSpent = 0;
        }
    }
}
