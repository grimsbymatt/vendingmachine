﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public class VendingMachine
    {
        public VendingMachineModel Model { get; set; }

        public static Dictionary<decimal, PhysicalCoin> KnownCoinsWithValues { get; set; }

        private string _decimalFormat = "0.#0";

        public VendingMachine()
        {
            KnownCoinsWithValues = new Dictionary<decimal, PhysicalCoin>();
        }

        public static void Main(string[] args)
        {
            var vmm = new VendingMachineModel();
            var vm = new VendingMachine { Model = vmm };

            SetupCoins(vmm);
            SetupProducts(vmm);
            
            vm.Start();
        }

        public void Start()
        {
            var input = "";

            while (true)
            {
                if (input == "" && Model.TotalInsertedAmount == 0)
                {
                    Console.WriteLine("INSERT COIN");
                }
                else if (Model.AvailableProducts.Select(p => p.Id.ToString()).Contains(input))
                {
                    try
                    {
                        var id = int.Parse(input);
                        var response = Model.PurchaseProduct(id);

                        if (response)
                        {
                            Console.WriteLine("THANK YOU");
                            Console.WriteLine("Dispensing: " + 
                                Model.AvailableProducts.First(p => p.Id == id).Name);

                            GetMoney();
                            input = "";
                            continue;
                        }
                        else
                        {
                            Console.WriteLine("PRICE " + Model.AvailableProducts.First(p => p.Id == id).Price.ToString(_decimalFormat));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                }
                else if (KnownCoinsWithValues.Keys.Select(v => v.ToString(_decimalFormat)).Contains(input))
                {
                    var inputCoinValue = decimal.Parse(input);
                    Model.CoinInserted(KnownCoinsWithValues[inputCoinValue]);
                    DisplayAmountInserted();
                }
                else if (input == "cancel")
                {
                    GetMoney();
                    input = "";
                    continue;
                }
                else
                {
                    DisplayAmountInserted();
                }

                input = Console.ReadLine();
            }
        }

        private void GetMoney()
        {
            var change = Model.ReturnMoney();

            if (change.Any())
            {
                foreach (var coin in change)
                {
                    Console.WriteLine("Returning coin: " + coin.ValueInDollars.ToString(_decimalFormat));
                }
            }
        }

        private void DisplayAmountInserted()
        {
            Console.WriteLine(Model.TotalInsertedAmount.ToString(_decimalFormat));
        }

        private static void SetupCoins(VendingMachineModel vmm)
        {
            var radii = new decimal[] {10, 12, 14, 16, 18, 20, 22};
            var weights = new decimal[] {5, 6, 7, 8, 9, 10, 11};
            var values = new[] {0.01m, 0.02m, 0.05m, 0.1m, 0.25m, 0.5m, 1.00m};

            for (var i = 0; i < 7; i++)
            {
                var coin = new PhysicalCoin
                {
                    RadiusInMm = radii[i],
                    WeightInGrams = weights[i]
                };

                KnownCoinsWithValues.Add(values[i], coin);
                vmm.AddCoinSpecification(coin, values[i]);
            }
        }

        private static void SetupProducts(VendingMachineModel vmm)
        {
            vmm.AddAvailableProduct("cola", 1.00m);
            vmm.AddAvailableProduct("chips", 0.50m);
            vmm.AddAvailableProduct("candy", 0.65m);
        }
    }
}
