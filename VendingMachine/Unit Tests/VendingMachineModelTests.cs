﻿using System;
using Xunit;

namespace VendingMachine.Unit_Tests
{
    public class VendingMachineModelTests
    {
        [Fact]
        public void CoinInserted_AddSingleCoin_ReturnsTrueAndTotalEqualsCoinValue()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 10,
                WeightInGrams = 5
            };

            //act
            var result = sut.CoinInserted(coinToInsert);

            //assert
            Assert.True(result);
            Assert.Equal(0.01m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void CoinInserted_AddTwoDifferentCoins_ReturnsTrueAndTotalEqualsCoinsSumValue()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 10,
                WeightInGrams = 5
            };

            var coinToInsert2 = new PhysicalCoin
            {
                RadiusInMm = 12,
                WeightInGrams = 6
            };

            //act
            var result = sut.CoinInserted(coinToInsert);
            var result2 = sut.CoinInserted(coinToInsert2);

            //assert
            Assert.True(result);
            Assert.True(result2);
            Assert.Equal(0.03m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void CoinInserted_AddTwoSameCoins_ReturnsTrueAndTotalEqualsDoubleCoinValue()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 10,
                WeightInGrams = 5
            };

            var coinToInsert2 = new PhysicalCoin
            {
                RadiusInMm = 10,
                WeightInGrams = 5
            };

            //act
            var result = sut.CoinInserted(coinToInsert);
            var result2 = sut.CoinInserted(coinToInsert2);

            //assert
            Assert.True(result);
            Assert.True(result2);
            Assert.Equal(0.02m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void CoinInserted_AddUnknkownCoin_ReturnsFalseAndTotalValueZero()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 5,
                WeightInGrams = 10
            };

            //act
            var result = sut.CoinInserted(coinToInsert);

            //assert
            Assert.False(result);
            Assert.Equal(0.00m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void GetProduct_InsufficientCoins_ReturnsFalseAndTotalValueUnchanged()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 20,
                WeightInGrams = 10
            };

            sut.CoinInserted(coinToInsert);

            //act
            var result = sut.PurchaseProduct(0);

            //assert
            Assert.False(result);
            Assert.Equal(0.50m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void GetProduct_SufficientCoins_ReturnsTrueAndTotalValueChanged()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 22,
                WeightInGrams = 11
            };

            sut.CoinInserted(coinToInsert);

            //act
            var result = sut.PurchaseProduct(1);

            //assert
            Assert.True(result);
            Assert.Equal(0.50m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void GetProduct_IncorrectId_ThrowsException()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 22,
                WeightInGrams = 11
            };

            sut.CoinInserted(coinToInsert);

            //act // assert
            Assert.Throws<Exception>(() => sut.PurchaseProduct(3));
        }

        [Fact]
        public void ReturnMoney_NoCoinsInserted_ReturnsEmptyListAndTotalZero()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);


            //act
            var result = sut.ReturnMoney();

            //assert
            Assert.Empty(result);
            Assert.Equal(0m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void ReturnMoney_CoinsInsertedNoPurchase_ReturnsCoinsInsertedAndTotalZero()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 10,
                WeightInGrams = 5
            };

            var coinToInsert2 = new PhysicalCoin
            {
                RadiusInMm = 12,
                WeightInGrams = 6
            };

            sut.CoinInserted(coinToInsert);
            sut.CoinInserted(coinToInsert2);


            //act
            var result = sut.ReturnMoney();

            //assert
            Assert.Equal(2, result.Count);
            Assert.Equal(0.01m, result[0].ValueInDollars);
            Assert.Equal(0.02m, result[1].ValueInDollars);
            Assert.Equal(0m, sut.TotalInsertedAmount);
        }

        [Fact]
        public void ReturnMoney_CoinsInsertedAndPurchaseMade_ReturnsCorrectChangeAndTotalZero()
        {
            // arrange
            var sut = new VendingMachineModel();

            SetupCoins(sut);
            SetupProducts(sut);

            var coinToInsert = new PhysicalCoin
            {
                RadiusInMm = 22,
                WeightInGrams = 11
            };

            sut.CoinInserted(coinToInsert);

            sut.PurchaseProduct(2);


            //act
            var result = sut.ReturnMoney();

            //assert
            Assert.Equal(2, result.Count);
            Assert.Equal(0.25m, result[0].ValueInDollars);
            Assert.Equal(0.1m, result[1].ValueInDollars);
            Assert.Equal(0, sut.TotalInsertedAmount);
        }

        private static void SetupCoins(VendingMachineModel vmm)
        {
            var radii = new decimal[] { 10, 12, 14, 16, 18, 20, 22 };
            var weights = new decimal[] { 5, 6, 7, 8, 9, 10, 11 };
            var values = new[] { 0.01m, 0.02m, 0.05m, 0.1m, 0.25m, 0.5m, 1.00m };

            for (var i = 0; i < 7; i++)
            {
                var coin = new PhysicalCoin
                {
                    RadiusInMm = radii[i],
                    WeightInGrams = weights[i]
                };

                vmm.AddCoinSpecification(coin, values[i]);
            }
        }

        private static void SetupProducts(VendingMachineModel vmm)
        {
            vmm.AddAvailableProduct("cola", 1.00m);
            vmm.AddAvailableProduct("chips", 0.50m);
            vmm.AddAvailableProduct("candy", 0.65m);
        }
    }
}
