# Vending Machine #

This project can be run simply by opening the solution file in the root folder and hitting start.

I implemented a unit tested vending machine model in VendingMachineModel.cs (unit tests in the Unit Tests folder). 
I wrote unit tests for the key methods and scenarios in the model.

I also implemented a simple machine in VendingMachine.cs, which demonstrates the model in action, while simulating the physical operations of the machine
itself and providing the required output to the user.

To use the machine, simply enter a coin value (0.01, 0.02, 0.05, 0.10, 0.25, 0.50 or 1.00) and hit enter to insert a coin. If the value format is not correct as listed here, the coin value won't be added.

To attempt a purchase, enter the id for the required item (0, 1 or 2 for cola, chips and candy, respectively) and hit enter.

To cancel and return your coins, type 'cancel' and hit enter.

Please note that this solution was written quite quickly at short notice and is by no means production-ready code! Further stuff I would do include: proper layering with interfaces, IoC/DI, error logging.